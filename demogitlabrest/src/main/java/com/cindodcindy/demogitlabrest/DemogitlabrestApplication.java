package com.cindodcindy.demogitlabrest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemogitlabrestApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemogitlabrestApplication.class, args);
	}

}
